FROM python:3.8

RUN apt update

RUN mkdir /srv/project
WORKDIR /srv/project

COPY ./myproject ./myproject
COPY ./commands ./commands
COPY ./requirements.txt ./requirements.txt

RUN pip install -r requirements.txt

CMD ["bash"]
